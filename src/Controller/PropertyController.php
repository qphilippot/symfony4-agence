<?php 

namespace App\Controller;

use App\Repository\PropertyRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Property;
use Doctrine\Common\Persistence\ObjectManager;

class PropertyController extends AbstractController {
    private $repository;


    public function __construct(
        PropertyRepository $repository,
        ObjectManager $em
        ) {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function index(): Response {
    //     $property = new Property();
    //     $property
    //         ->setTitle('mon premier bien')
    //         ->setPrice(200000)
    //         ->setRooms(4)
    //         ->setBedrooms(3)
    //         ->setFloor(4)
    //         ->setSurface(60)
    //         ->setCity('Montpellier')
    //         ->setPostalCode('34000')
    //         ->setAddress('123 somestreet')
    //         ->setDescription("Mah qu'il est beau !")
    //         ->setHeat(1);

    // $manager = $this->getDoctrine()->getManager();
    // $manager->persist($property);
    // $manager->flush();
    
    //$property = $this->repository->findAllVisible();
    // dump($property);
    
        return $this->render('property/index.html.twig', [
            'current_menu' => 'properties'
        ]);
    }

public function show(Property $property, String $slug) : Response {
    // $property = $this->repository->find($id);
    $p_slug = $property->getSlug();
    if ($p_slug !== $slug) {
        return $this->redirectToRoute('property.show', [
            'id' => $property->getId(),
            'slug' => $p_slug
        ], 301);
    }

    return $this->render('property/show.html.twig', [
        "property" => $property,
        'current_menu' => 'properties'
    ]);
}

}