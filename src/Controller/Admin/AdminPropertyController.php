<?php

namespace App\Controller\Admin;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\PropertyType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminPropertyController extends AbstractController  {
    /**
     * @var PropertyRepository
     */
    private $repository;

    public function __construct(PropertyRepository $repository, ObjectManager $em) {
        $this->repository = $repository;
        $this->em = $em; 
    }
    
    public function index() {
        $properties = $this->repository->findAll();
        return $this->render('admin/property/index.html.twig', compact('properties'));
    }


    public function edit(Property $property, Request $request) {
        // $form = $this->createForm(PropertyType::class, $property);
        // $form->handleRequest($request);

        // if (
        //     $form->isSubmitted() &&
        //     $form->isValid()
        // ) {
        //     $this->em->flush();
        //     return $this->redirectToRoute('admin.property.index');
        // }

        $isSaved = $this->saveFromRequest(
            $property,
            $request
        );

        if ($isSaved !== null) {

            $this->addFlash('success', 'modifications enregistrées');
            return $isSaved;
        }

        else {

            return $this->render('admin/property/edit.html.twig', [
                'property' => $property,
                'form' => $this->createForm(PropertyType::class, $property)->createView( )
            ]);
        }
    }

    public function create(Request $request) {
        $property = new Property();
        $isSaved = $this->saveFromRequest(
            $property,
            $request,
            true
       );

        if ($isSaved !== null) {

            $this->addFlash('success', 'création réussie');
           return $isSaved;
        }

       else {
            return $this->render('admin/property/create.html.twig', [
                'property' => $property,
                'form' => $this->createForm(PropertyType::class, $property)->createView( )
            ]);
       }
    }


    private function saveFromRequest(Property $property, Request $request, bool $isNew = false, string $fallback =  'admin.property.index') {
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if (
            $form->isSubmitted() &&
            $form->isValid()
        ) {
            if ($isNew === true) {
                $this->em->persist($property);
            }

            $this->em->flush();
            return $this->redirectToRoute($fallback);
        }

        else {
            return null;
        }
    }

    public function delete(Property $property, Request $req) {
        if ($this->isCsrfTokenValid('delete' . $property->getId(), $req->get('_token'))) {
            $this->em->remove($property);
            $this->addFlash('success', $property->getTitle() . ' supprimé');
            $this->em->flush();
            // return new Response('suppression');
            // $this->redirectToRoute('admin.property.index');
        }

        return $this->redirectToRoute('admin.property.index');
    }
}